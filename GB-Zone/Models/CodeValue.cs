﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrinGlobal.Zone.Models
{
    public class CodeValue
    {
        public string Value { get; set; }
        public string Title { get; set; }
    }
}