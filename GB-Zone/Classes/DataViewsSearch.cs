﻿using DevExpress.Web.Mvc;
using GrinGlobal.Zone.Helpers;
using GrinGlobal.Zone.Models;
using System;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Collections;

namespace GrinGlobal.Zone.Classes
{
    public class DataViewsSearch
    {
        //private string _value { get; set; }
        //private string _cropId { get; set; }
        //private string _viewSelected { get; set; }

        void DataviewSearch() { }

        /// <summary>
        /// Get data from GRIN-Global Server
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="moduleId"></param>
        /// <param name="formId"></param>
        /// <param name="fieldId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DataTable GetData(string serverId, string moduleId, string formId, string fieldId, string value)
        {
            int nBracket = 0;
            int nIndex = 0;
            string colName = string.Empty;

            //read the project
            XElement service = Settings.Form(serverId, moduleId, formId)
                                  .Elements("field")
                                  .Where(c => (string)c.Attribute("id") == fieldId).FirstOrDefault();

            //extract settings from Setting.xml
            string urlService = service.Parent.Parent.Parent.Attribute("url").Value.ToString();
            string dataviewName = service.Element("actions").Element("parameters").Element("dataviewName").Value;
            bool suppressExceptions = bool.Parse(service.Element("actions").Element("parameters").Element("suppressExceptions").Value);
            int offset = int.Parse(service.Element("actions").Element("parameters").Element("offset").Value);
            int limit = int.Parse(service.Element("actions").Element("parameters").Element("limit").Value);
            string options = service.Element("actions").Element("parameters").Element("options").Value;

            //put the value in the delimitedParameterList
            string delimitedParams = service.Element("actions").Element("parameters").Element("delimitedParameterList").Value;
            Char separator = (char) Convert.ToInt32(service.Element("actions").Element("parameters").Element("separator").Value);
                        
            var arrValue = value.Split(separator);

            while ((nBracket = delimitedParams.IndexOf("{0}", nBracket)) != -1)
            {
                var paramValue = "";
                if (nIndex < arrValue.Length)
                    paramValue = arrValue[nIndex];
                
                delimitedParams = delimitedParams.Remove(nBracket, 3).Insert(nBracket, paramValue);
                nBracket++;
                nIndex++;
            }

            GGZoneModel ggZoneModel = new GGZoneModel();

            //invoke model requesting the datatable
            DataSet ds = ggZoneModel.GetData(urlService, suppressExceptions, dataviewName, delimitedParams, offset, limit, options);

            //remove or add column
            foreach (DataColumn col in ds.Tables[dataviewName].Columns)
            {
                if (col.ExtendedProperties["is_visible"].ToString() == "N")
                {
                    col.ColumnMapping = MappingType.Hidden;
                }

                XElement column = (from e in service.Element("actions").Elements("columns").Descendants("column")
                                where e.Value.Trim().ToUpper() == col.ColumnName.Trim().ToUpper()
                                select e).FirstOrDefault();

                if (column != null)
                {
                    //create extendproperties
                    if (column.Attribute("header") != null && bool.Parse(column.Attribute("header").Value))
                    {
                        col.ExtendedProperties.Add("is_header", true);
                    }
                    if (column.Attribute("link") != null && bool.Parse(column.Attribute("link").Value))
                    {
                        col.ExtendedProperties.Add("moduleRef", column.Attribute("moduleRef").Value);
                        col.ExtendedProperties.Add("formRef", column.Attribute("formRef").Value);
                        col.ExtendedProperties.Add("fieldRef", column.Attribute("fieldRef").Value);
                        col.ExtendedProperties.Add("colRef", column.Attribute("colRef").Value);
                    }
                }
            }

            
            if(service.Element("actions").Element("extendedProperties").Element("masterDetail") != null)
            {
                ds.Tables[dataviewName].ExtendedProperties.Add("masterDetail", true);
                ds.Tables[dataviewName].ExtendedProperties.Add("actionName", service.Element("actions").Element("extendedProperties").Element("masterDetail").Attribute("actionName").Value);
                ds.Tables[dataviewName].ExtendedProperties.Add("moduleRef", service.Element("actions").Element("extendedProperties").Element("masterDetail").Attribute("moduleRef").Value);
                ds.Tables[dataviewName].ExtendedProperties.Add("formRef", service.Element("actions").Element("extendedProperties").Element("masterDetail").Attribute("formRef").Value);
                ds.Tables[dataviewName].ExtendedProperties.Add("fieldRef", service.Element("actions").Element("extendedProperties").Element("masterDetail").Attribute("fieldRef").Value);
                ds.Tables[dataviewName].ExtendedProperties.Add("colRef", service.Element("actions").Element("extendedProperties").Element("masterDetail").Attribute("colRef").Value);
            }

            return ds.Tables[dataviewName];

        }

        /// <summary>
        /// Save data to GRIN-Global Server
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="moduleId"></param>
        /// <param name="formId"></param>
        /// <param name="fieldId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DataTable SaveData(string serverId, string moduleId, string formId, string fieldId, string value)
        {
            int nBracket = 0;
            int nIndex = 0;
            string colName = string.Empty;

            //read the project
            XElement service = Settings.Form(serverId, moduleId, formId)
                                  .Elements("field")
                                  .Where(c => (string)c.Attribute("id") == fieldId).FirstOrDefault();

            //extract settings from Setting.xml
            string urlService = service.Parent.Parent.Parent.Attribute("url").Value.ToString();
            string dataviewName = service.Element("actions").Element("parameters").Element("dataviewName").Value;
            bool suppressExceptions = bool.Parse(service.Element("actions").Element("parameters").Element("suppressExceptions").Value);
            int offset = int.Parse(service.Element("actions").Element("parameters").Element("offset").Value);
            int limit = int.Parse(service.Element("actions").Element("parameters").Element("limit").Value);
            string options = service.Element("actions").Element("parameters").Element("options").Value;

            //put the value in the delimitedParameterList
            string delimitedParams = service.Element("actions").Element("parameters").Element("delimitedParameterList").Value;
            Char separator = (char)Convert.ToInt32(service.Element("actions").Element("parameters").Element("separator").Value);

            var arrValue = value.Split(separator);

            while ((nBracket = delimitedParams.IndexOf("{0}", nBracket)) != -1)
            {
                var paramValue = "";
                if (nIndex < arrValue.Length)
                    paramValue = arrValue[nIndex];

                delimitedParams = delimitedParams.Remove(nBracket, 3).Insert(nBracket, paramValue);
                nBracket++;
                nIndex++;
            }

            GGZoneModel ggZoneModel = new GGZoneModel();

            //invoke model requesting the datatable
            DataSet oldds = ggZoneModel.GetData(urlService, suppressExceptions, dataviewName, delimitedParams, offset, limit, options);

            DataTable model = oldds.Tables[dataviewName];

            var primaryKey = model.PrimaryKey[0].ColumnName;

            DataRow[] dr = model.Select(primaryKey + " = " + GridViewExtension.GetEditValue<dynamic>(primaryKey));

            foreach (DataColumn col in model.Columns)
            {
                if (!col.ReadOnly)
                {
                    var val = GridViewExtension.GetEditValue<dynamic>(col.ColumnName);

                    if (val != null)
                    {
                        dr[0][col.ColumnName] = val;
                    }
                }
            }

            DataSet result = ggZoneModel.SaveData(urlService, suppressExceptions, oldds, options);
            
            return result.Tables[dataviewName];
        }

        /// <summary>
        /// Get Code Value List by code group
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public static IEnumerable GetCategories(string serverId, string group)
        {
            XElement service = Settings.Server(serverId);
            
            string urlService = service.Attribute("url").Value.ToString();
            string dataviewName = "get_code_value_list";
            bool suppressExceptions = false;
            int offset = 0;
            int limit = 0;
            string options = "1";
            string delimitedParams = ":groupname=" + group;

            GGZoneModel ggZoneModel = new GGZoneModel();

            //invoke model requesting the datatable
            DataSet ds = ggZoneModel.GetData(urlService, suppressExceptions, dataviewName, delimitedParams, offset, limit, options);

            DataTable dataTableName = ds.Tables[dataviewName];

            List<CodeValue> listName = dataTableName.AsEnumerable().Select(m => new CodeValue()
            {
                Value = m.Field<string>("value"),
                Title = m.Field<string>("title"),
            }).ToList();

            return listName;
        }
    }
}